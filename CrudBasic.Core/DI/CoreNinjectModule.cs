﻿using CrudBasic.Core.Interfaces.Services;
using CrudBasic.Core.Services;
using Ninject.Modules;

namespace CrudBasic.Core.DI
{
    public class CoreNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IClientesServices>().To<ClientesServices>();
            Bind<IProdutosService>().To<ProdutosServices>();
            Bind<IVendasServices>().To<VendasServices>();
        }
    }
}

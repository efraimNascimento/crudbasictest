﻿using CrudBasic.Core.Models;

namespace CrudBasic.Core.Interfaces.Repositories
{
    public interface IProdutoRepository: IRepositoryBase<Produto>
    {}
}

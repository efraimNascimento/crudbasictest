﻿using CrudBasic.Core.Models;

namespace CrudBasic.Core.Interfaces.Repositories
{
    public interface IVendaRepository: IRepositoryBase<Venda>
    {}
}

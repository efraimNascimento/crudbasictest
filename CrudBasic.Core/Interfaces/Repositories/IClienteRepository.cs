﻿using CrudBasic.Core.Models;

namespace CrudBasic.Core.Interfaces.Repositories
{
    public interface IClienteRepository: IRepositoryBase<Cliente>
    {}
}

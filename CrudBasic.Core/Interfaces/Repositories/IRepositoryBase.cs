﻿using CrudBasic.Core.Models;
using System.Collections.Generic;

namespace CrudBasic.Core.Interfaces.Repositories
{
    public interface IRepositoryBase<T> where T : ModelBase
    {
        T GetById(long id);
        IEnumerable<T> ListAll();
        T Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}

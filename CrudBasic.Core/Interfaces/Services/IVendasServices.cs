﻿using CrudBasic.Core.DTO;
using System.Collections.Generic;

namespace CrudBasic.Core.Interfaces.Services
{
    public interface IVendasServices
    {
        VendaDTO Criar(VendaDTO cliente);
        VendaDTO Alterar(VendaDTO cliente);
        void Excluir(long Id);
        IEnumerable<VendaDTO> ListarTodos();
    }
}

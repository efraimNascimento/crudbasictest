﻿using CrudBasic.Core.DTO;
using System.Collections.Generic;

namespace CrudBasic.Core.Interfaces.Services
{
    public interface IClientesServices
    {
        ClienteDTO Criar(ClienteDTO cliente);
        ClienteDTO Alterar(ClienteDTO cliente);
        void Excluir(long Id);
        IEnumerable<ClienteDTO> ListarTodos();
        bool EhClienteValido(long Id);
        ClienteDTO GetById(long Id);
    }
}

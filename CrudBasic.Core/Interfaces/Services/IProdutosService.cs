﻿using CrudBasic.Core.DTO;
using System.Collections.Generic;

namespace CrudBasic.Core.Interfaces.Services
{
    public interface IProdutosService
    {
        ProdutoDTO Criar(ProdutoDTO cliente);
        ProdutoDTO Alterar(ProdutoDTO cliente);
        void Excluir(long Id);
        IEnumerable<ProdutoDTO> ListarTodos();
        bool EhProdutoValido(long id);
        ProdutoDTO GetById(long Id);
    }
}

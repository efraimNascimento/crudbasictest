﻿using AutoMapper;
using CrudBasic.Core.DTO;
using CrudBasic.Core.Models;

namespace CrudBasic.Core.Mapas
{
    public class ClienteProfile : Profile
    {
        public ClienteProfile() => CreateMap<Cliente, ClienteDTO>().ReverseMap();
    }
}

﻿using AutoMapper;
using CrudBasic.Core.DTO;
using CrudBasic.Core.Models;

namespace CrudBasic.Core.Mapas
{
    public class ProdutoProfile : Profile
    {
        public ProdutoProfile() => CreateMap<Produto, ProdutoDTO>().ReverseMap();
    }
}

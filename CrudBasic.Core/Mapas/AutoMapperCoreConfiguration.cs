﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace CrudBasic.Core.Mapas
{
    public class AutoMapperCoreConfiguration
    {
        public static void Configure(IEnumerable<Profile> profiles) => Mapper.Initialize(config => { AddProfiles(config, profiles); AddProfilesCore(config); });

        private static void AddProfiles(IMapperConfigurationExpression configuration, IEnumerable<Profile> profiles) => profiles.ToList().ForEach(configuration.AddProfile);

        private static void AddProfilesCore(IMapperConfigurationExpression configuration)
        {
            configuration.AddProfile<ClienteProfile>();
            configuration.AddProfile<ProdutoProfile>();
            configuration.AddProfile<VendasProfile>();
        }
    }
}

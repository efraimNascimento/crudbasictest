﻿using AutoMapper;
using CrudBasic.Core.DTO;
using CrudBasic.Core.Models;

namespace CrudBasic.Core.Mapas
{
    public class VendasProfile : Profile
    {
        public VendasProfile()
        {
            CreateMap<Venda, VendaDTO>().ReverseMap();
            CreateMap<VendaDetalhe, VendaDetalheDTO>().ReverseMap();
        }
    }
}

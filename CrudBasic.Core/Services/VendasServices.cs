﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CrudBasic.Core.DTO;
using CrudBasic.Core.Exceptions;
using CrudBasic.Core.Interfaces.Repositories;
using CrudBasic.Core.Interfaces.Services;
using CrudBasic.Core.Models;

namespace CrudBasic.Core.Services
{
    public class VendasServices : IVendasServices
    {
        private readonly IVendaRepository _vendasRepository;
        private readonly IClientesServices _clientesServices;
        private readonly IProdutosService _produtosService;
        public VendasServices(IVendaRepository vendaRepository, IClientesServices clientesServices, IProdutosService produtosService)
        {
            _vendasRepository = vendaRepository;
            _clientesServices = clientesServices;
            _produtosService = produtosService;
        }

        public VendaDTO Alterar(VendaDTO venda)
        {
            try
            {
                if (!EhVendaValida(venda.Id))
                    throw new VendaNaoLocalizadoException();

                var vendaModel = Mapper.Map<Venda>(venda);

                ValidarModeloAntesDeSalvar(vendaModel);
                _vendasRepository.Update(vendaModel);
                return venda;
            }
            catch
            {
                throw new NaoFoiPossivelAlterarVendaException();
            }
        }

        public VendaDTO Criar(VendaDTO venda)
        {
            try
            {
                var vendaModel = Mapper.Map<Venda>(venda);
                ValidarModeloAntesDeSalvar(vendaModel);
                return Mapper.Map<VendaDTO>(_vendasRepository.Add(vendaModel));
            }
            catch 
            {
                throw new NaoFoiPossivelCriarVendaException();
            }
        }

        public void Excluir(long Id)
        {
            try
            {
                var venda = _vendasRepository.GetById(Id);
                if (venda == null)
                    throw new VendaNaoLocalizadoException();
                _vendasRepository.Delete(venda);
            }
            catch
            {
                throw new NaoFoiPossivelExcluirVendaException();
            }
        }

        public IEnumerable<VendaDTO> ListarTodos()
        {
            try
            {
                return Mapper.Map<IEnumerable<VendaDTO>>(_vendasRepository.ListAll());
            }
            catch
            {
                throw new NaoFoiPossivelRecuperarOsRegistrosException();
            }
        }

        private void ValidarModeloAntesDeSalvar(Venda vendaModel)
        {
            if (!_clientesServices.EhClienteValido(vendaModel.ClienteId))
                throw new ClienteNaoLocalizadoException();
            if (!vendaModel.DetalheDaVenda.Any())
                throw new NenhumaVendaDetalheInformada();
            ValidarItensDaVenda(vendaModel);
        }

        private void ValidarItensDaVenda(Venda vendaModel)
        {
            foreach (var item in vendaModel.DetalheDaVenda)
            {
                if (item.Id == 0)
                    throw new UmOuMaisProdutosNaoInformadosException();
                if (!_produtosService.EhProdutoValido(item.Id))
                    throw new UmOuMaisProdutosNaoEncontradosException();
            }
        }

        private bool EhVendaValida(long id) => _vendasRepository.GetById(id) != null;
    }
}

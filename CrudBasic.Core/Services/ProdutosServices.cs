﻿using System.Collections.Generic;
using AutoMapper;
using CrudBasic.Core.DTO;
using CrudBasic.Core.Exceptions;
using CrudBasic.Core.Interfaces.Repositories;
using CrudBasic.Core.Interfaces.Services;
using CrudBasic.Core.Models;

namespace CrudBasic.Core.Services
{
    public class ProdutosServices : IProdutosService
    {
        private readonly IProdutoRepository _produtoRepository;

        public ProdutosServices(IProdutoRepository produtoRepository) => _produtoRepository = produtoRepository;

        public ProdutoDTO Alterar(ProdutoDTO produto)
        {
            try
            {
                _produtoRepository.Update(Mapper.Map<Produto>(produto));
                return produto;
            }
            catch
            {
                throw new NaoFoiPossivelAlterarProdutoException();
            }
        }

        public ProdutoDTO Criar(ProdutoDTO produto)
        {
            try
            {
                _produtoRepository.Add(Mapper.Map<Produto>(produto));
                return produto;
            }
            catch
            {
                throw new NaoFoiPossivelCriarProdutoException();
            }
        }

        public void Excluir(long Id)
        {
            try
            {
                var produto = _produtoRepository.GetById(Id);
                if (produto == null)
                    throw new ProdutoNaoLocalizadoException();

                _produtoRepository.Delete(produto);
            }
            catch
            {
                throw new NaoFoiPossivelExcluirProdutoException();
            }
        }

        public IEnumerable<ProdutoDTO> ListarTodos()
        {
            try
            {
                return Mapper.Map<IEnumerable<ProdutoDTO>>(_produtoRepository.ListAll());
            }
            catch
            {
                throw new NaoFoiPossivelRecuperarOsRegistrosException();
            }
        }

        public bool EhProdutoValido(long id) => _produtoRepository.GetById(id) != null;

        public ProdutoDTO GetById(long Id)
        {
            try
            {
                var produto = _produtoRepository.GetById(Id);
                if (produto == null)
                    throw new ProdutoNaoLocalizadoException();
                return Mapper.Map<ProdutoDTO>(produto);
            }
            catch
            {
                throw new NaoFoiPossivelRecuperarOsRegistrosException();
            }
        }

    }
}

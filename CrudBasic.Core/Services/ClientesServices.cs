﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CrudBasic.Core.DTO;
using CrudBasic.Core.Exceptions;
using CrudBasic.Core.Interfaces.Repositories;
using CrudBasic.Core.Interfaces.Services;
using CrudBasic.Core.Models;

namespace CrudBasic.Core.Services
{
    public class ClientesServices : IClientesServices
    {
        private readonly IClienteRepository _clienteRepository;

        public ClientesServices(IClienteRepository clienteRepository) => _clienteRepository = clienteRepository;

        public ClienteDTO Alterar(ClienteDTO cliente)
        {
            try
            {
                _clienteRepository.Update(Mapper.Map<Cliente>(cliente));
                return cliente;
            }
            catch
            {
                throw new NaoFoiPossivelAlterarClienteException();
            }
        }

        public ClienteDTO Criar(ClienteDTO cliente)
        {
            try
            {
                var clienteCriado = _clienteRepository.Add(Mapper.Map<Cliente>(cliente));
                return Mapper.Map<ClienteDTO>(clienteCriado);
            }
            catch
            {
                throw new NaoFoiPossivelCriarClienteException();
            }
        }

        public void Excluir(long Id)
        {
            try
            {
                var cliente = _clienteRepository.GetById(Id);
                if (cliente == null)
                    throw new ClienteNaoLocalizadoException();

                _clienteRepository.Delete(cliente);
            }
            catch
            {
                throw new NaoFoiPossivelCriarClienteException();
            }
        }

        public IEnumerable<ClienteDTO> ListarTodos()        
        {
            try
            {
                return Mapper.Map<IEnumerable<ClienteDTO>>(_clienteRepository.ListAll());
            }
            catch
            {
                throw new NaoFoiPossivelRecuperarOsRegistrosException();
            }
        }

        public bool EhClienteValido(long Id) => _clienteRepository.GetById(Id) != null;

        public ClienteDTO GetById(long Id)
        {
            try
            {
                var cliente = _clienteRepository.GetById(Id);
                if (cliente == null)
                    throw new ClienteNaoLocalizadoException();
                return Mapper.Map<ClienteDTO>(cliente);
            }
            catch
            {
                throw new NaoFoiPossivelRecuperarOsRegistrosException();
            }
        }

    }
}

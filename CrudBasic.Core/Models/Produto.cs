﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CrudBasic.Core.Models
{
    [Table("Produtos")]
    public class Produto: ModelBase
    {
        public string CodigoBarra { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
    }
}

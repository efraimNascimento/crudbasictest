﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CrudBasic.Core.Models
{
    [Table("VendaDetalhes")]
    public class VendaDetalhe : ModelBase
    {
        public long VendaId { get; set; }
        public virtual Venda Venda {get; set;}
        public long ProdutoId { get; set; }
        public virtual Produto Produto { get; set; }
        public decimal Desconto { get; set; }
        public decimal ValorTotal { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CrudBasic.Core.Models
{
    [Table("Clientes")]
    public class Cliente: ModelBase
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Uf { get; set; }
    }
}

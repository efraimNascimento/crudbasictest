﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CrudBasic.Core.Models
{
    [Table("Vendas")]
    public class Venda: ModelBase
    {
        public DateTime Data { get; set; }
        public long ClienteId { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual IEnumerable<VendaDetalhe> DetalheDaVenda { get; set; }
        public decimal TotalVenda { get; set; }
    }
}

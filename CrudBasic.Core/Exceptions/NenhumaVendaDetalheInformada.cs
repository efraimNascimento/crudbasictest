﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class NenhumaVendaDetalheInformada : Exception
    {
        public NenhumaVendaDetalheInformada() : base("É necessário informar pelo menos um detalhe de venda.")
        { }
    }
}

﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class VendaNaoLocalizadoException : Exception
    {
        public VendaNaoLocalizadoException() : base("Venda Não Localizada.")
        { }
    }
}

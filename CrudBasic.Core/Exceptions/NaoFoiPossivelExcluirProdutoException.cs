﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class NaoFoiPossivelExcluirProdutoException : Exception
    {
        public NaoFoiPossivelExcluirProdutoException() : base("Erro ao excluir o produto.")
        { }
    }
}

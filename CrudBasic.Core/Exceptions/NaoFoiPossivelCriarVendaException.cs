﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class NaoFoiPossivelCriarVendaException : Exception
    {
        public NaoFoiPossivelCriarVendaException() : base("Erro ao criar uma nova venda.")
        { }
    }
}

﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class NaoFoiPossivelExcluirVendaException : Exception
    {
        public NaoFoiPossivelExcluirVendaException() : base("Erro ao excluir a Venda.")
        { }
    }
}

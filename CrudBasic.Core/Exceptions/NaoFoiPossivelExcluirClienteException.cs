﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class NaoFoiPossivelExcluirClienteException : Exception
    {
        public NaoFoiPossivelExcluirClienteException() : base("Erro ao excluir o cliente.")
        { }
    }
}

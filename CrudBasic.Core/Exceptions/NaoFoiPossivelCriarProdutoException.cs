﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class NaoFoiPossivelCriarProdutoException : Exception
    {
        public NaoFoiPossivelCriarProdutoException() : base("Erro ao criar um novo produto.")
        { }
    }
}

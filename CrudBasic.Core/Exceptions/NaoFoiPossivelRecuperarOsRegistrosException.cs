﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class NaoFoiPossivelRecuperarOsRegistrosException : Exception
    {
        public NaoFoiPossivelRecuperarOsRegistrosException() : base("Não foi possível recuperar os registros.")
        { }
    }
}

﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class ClienteNaoLocalizadoException : Exception
    {
        public ClienteNaoLocalizadoException() : base("Cliente Não Localizado.")
        { }
    }
}

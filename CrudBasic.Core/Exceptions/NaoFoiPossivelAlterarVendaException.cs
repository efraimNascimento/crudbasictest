﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class NaoFoiPossivelAlterarVendaException : Exception
    {
        public NaoFoiPossivelAlterarVendaException() : base("Erro ao alterar uma venda.")
        { }
    }
}

﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class UmOuMaisProdutosNaoInformadosException : Exception
    {
        public UmOuMaisProdutosNaoInformadosException() : base("Um ou mais produtos não foram informados no detalhe de venda.")
        { }
    }
}

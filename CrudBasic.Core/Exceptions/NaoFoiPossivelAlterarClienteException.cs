﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class NaoFoiPossivelAlterarClienteException : Exception
    {
        public NaoFoiPossivelAlterarClienteException() : base("Erro ao alterar os dados de um cliente")
        { }
    }
}

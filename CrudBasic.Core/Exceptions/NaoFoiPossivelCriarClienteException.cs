﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class NaoFoiPossivelCriarClienteException : Exception
    {
        public NaoFoiPossivelCriarClienteException() : base("Erro ao criar um novo cliente.")
        { }
    }
}

﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class ProdutoNaoLocalizadoException : Exception
    {
        public ProdutoNaoLocalizadoException() : base("Produto Não Localizado.")
        { }
    }
}

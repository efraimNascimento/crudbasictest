﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class UmOuMaisProdutosNaoEncontradosException : Exception
    {
        public UmOuMaisProdutosNaoEncontradosException() : base("Um ou mais produtos não foram encontrados no detalhe de venda.")
        { }
    }
}

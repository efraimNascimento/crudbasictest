﻿using System;

namespace CrudBasic.Core.Exceptions
{
    public class NaoFoiPossivelAlterarProdutoException : Exception
    {
        public NaoFoiPossivelAlterarProdutoException() : base("Erro ao alterar os dados de um produto.")
        { }
    }
}

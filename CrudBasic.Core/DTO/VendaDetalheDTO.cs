﻿namespace CrudBasic.Core.DTO
{
    public class VendaDetalheDTO
    {
        public long Id { get; set; }
        public virtual VendaDTO Venda { get; set; }
        public virtual ProdutoDTO Produto { get; set; }
        public decimal Desconto { get; set; }
        public decimal ValorTotal { get; set; }
    }
}

﻿namespace CrudBasic.Core.DTO
{
    public class ProdutoDTO
    {
        public long Id { get; set; }
        public string CodigoBarra { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CrudBasic.Core.DTO
{
    public class VendaDTO
    {
        public long Id { get; set; }
        public DateTime Data { get; set; }
        public ClienteDTO Cliente { get; set; }
        public IEnumerable<VendaDetalheDTO> DetalheDaVenda { get; set; }
        public decimal TotalVenda { get; set; }
    }
}

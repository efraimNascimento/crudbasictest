﻿using CrudBasic.Core.Interfaces.Repositories;
using CrudBasic.Infrastructure.Data;
using CrudBasic.Infrastructure.Data.Repositories;
using Ninject.Modules;

namespace CrudBasic.Infrastructure.DI
{
    public class InfraNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<EFContext>();
            Bind<IClienteRepository>().To<ClienteRepository>();
            Bind<IProdutoRepository>().To<ProdutoRepository>();
            Bind<IVendaRepository>().To<VendaRepository>();
        }
    }
}

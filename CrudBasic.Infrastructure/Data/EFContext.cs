﻿using CrudBasic.Core.Models;
using CrudBasic.Infrastructure.Migrations;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace CrudBasic.Infrastructure.Data
{
    public class EFContext: DbContext, IUnitOfWork
    {
        private bool _rolledback;
        public DbSet<Cliente> TodosClientes { get; set; }
        public DbSet<Produto> TodosProdutos { get; set; }
        public DbSet<Venda> TodasVendas { get; set; }
        public DbSet<VendaDetalhe> TodosVendaDetalhes { get; set; }

        public EFContext()
            : base(ConnectionString.Get)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<EFContext, Configuration>());
            Configuration.LazyLoadingEnabled = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) => modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

        public DbContextTransaction Transaction { get; private set; }

        public void Commit()
        {
            if (Transaction != null && !_rolledback)
            {
                Transaction.Commit();
                Transaction.Dispose();
                Transaction = null;
            }
        }

        public DbContextTransaction StartTransaction() => Transaction ?? (Transaction = Database.BeginTransaction());

        public DbContextTransaction StartTransaction(IsolationLevel isolationLevel)
            => Transaction ?? (Transaction = Database.BeginTransaction(isolationLevel));

        public void Rollback()
        {
            if (Transaction?.UnderlyingTransaction.Connection != null && !_rolledback)
            {
                Transaction.Rollback();
                _rolledback = true;
            }
        }

        public virtual void Save()
        {
            try
            {
                SaveChanges();
            }
            catch
            {
                Rollback();
                throw;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (Transaction != null && !_rolledback)
                Commit();

            if (!_rolledback)
                Save();
            else
                Rollback();

            base.Dispose(disposing);
        }
    }
}

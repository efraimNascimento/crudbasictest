﻿using CrudBasic.Core.Models;
using System.Data.Entity.ModelConfiguration;

namespace CrudBasic.Infrastructure.Data.Mapping
{
    public class ProdutoEntityTypeConfiguration: EntityTypeConfiguration<Produto>
    {
        public ProdutoEntityTypeConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.CodigoBarra).IsOptional();
            Property(x => x.Descricao).IsRequired();
            Property(x => x.Valor).IsRequired();
        }
    }
}

﻿using CrudBasic.Core.Models;
using System.Data.Entity.ModelConfiguration;

namespace CrudBasic.Infrastructure.Data.Mapping
{
    public class VendaEntityTypeConfiguration : EntityTypeConfiguration<Venda>
    {
        public VendaEntityTypeConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.Data).IsRequired();
            HasRequired(x => x.Cliente).WithMany().HasForeignKey(x => x.ClienteId).WillCascadeOnDelete(false);
            HasOptional(x => x.DetalheDaVenda).WithMany();
        }
    }
}

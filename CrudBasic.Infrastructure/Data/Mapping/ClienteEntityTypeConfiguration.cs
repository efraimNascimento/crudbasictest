﻿using CrudBasic.Core.Models;
using System.Data.Entity.ModelConfiguration;

namespace CrudBasic.Infrastructure.Data.Mapping
{
    public class ClienteEntityTypeConfiguration : EntityTypeConfiguration<Cliente>
    {
        public ClienteEntityTypeConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.Nome).IsRequired();
            Property(x => x.Email).IsRequired();
            Property(x => x.Endereco).IsOptional();
            Property(x => x.Bairro).IsOptional();
            Property(x => x.Cidade).IsOptional();
            Property(x => x.Uf).IsOptional();
        }
    }
}
﻿using CrudBasic.Core.Models;
using System.Data.Entity.ModelConfiguration;

namespace CrudBasic.Infrastructure.Data.Mapping
{
    public class VendaDetalheEntityTypeConfiguration: EntityTypeConfiguration<VendaDetalhe>
    {
        public VendaDetalheEntityTypeConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.Desconto).IsOptional();
            Property(x => x.ValorTotal).IsRequired();
            HasRequired(x => x.Produto).WithMany().HasForeignKey(x => x.ProdutoId);
        }
    }
}

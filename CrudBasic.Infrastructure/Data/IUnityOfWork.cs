﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace CrudBasic.Infrastructure.Data
{
    public interface IUnitOfWork : IDisposable, IObjectContextAdapter
    {
        Database Database { get; }
        DbChangeTracker ChangeTracker { get; }
        DbContextTransaction Transaction { get; }
        DbSet Set(Type entityType);
        DbSet<T> Set<T>() where T : class;
        int SaveChanges();
        void Commit();
        DbContextTransaction StartTransaction();
        DbContextTransaction StartTransaction(IsolationLevel isolationLevel);
        void Rollback();
        void Save();
        DbEntityEntry Entry(object targetValue);
    }
}

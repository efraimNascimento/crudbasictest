﻿using CrudBasic.Core.Interfaces.Repositories;
using CrudBasic.Core.Models;

namespace CrudBasic.Infrastructure.Data.Repositories
{
    public class ProdutoRepository : RepositoryBase<Produto>, IProdutoRepository
    {
        public ProdutoRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {}
    }
}

﻿using CrudBasic.Core.Interfaces.Repositories;
using CrudBasic.Core.Models;

namespace CrudBasic.Infrastructure.Data.Repositories
{
    public class ClienteRepository : RepositoryBase<Cliente>, IClienteRepository
    {
        public ClienteRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {}
    }
}

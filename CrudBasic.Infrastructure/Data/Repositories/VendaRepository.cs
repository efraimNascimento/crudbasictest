﻿using CrudBasic.Core.Interfaces.Repositories;
using CrudBasic.Core.Models;

namespace CrudBasic.Infrastructure.Data.Repositories
{
    public class VendaRepository : RepositoryBase<Venda>, IVendaRepository
    {
        public VendaRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {}
    }
}

﻿using CrudBasic.Core.Interfaces.Repositories;
using CrudBasic.Core.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CrudBasic.Infrastructure.Data.Repositories
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : ModelBase
    {
        protected readonly IUnitOfWork _unitOfWork;

        public RepositoryBase(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public virtual T GetById(long id) => _unitOfWork.Set<T>().Find(id);

        public IEnumerable<T> ListAll() => _unitOfWork.Set<T>().AsEnumerable();

        public T Add(T entity)
        {
            _unitOfWork.Set<T>().Add(entity);
            _unitOfWork.SaveChanges();
            return entity;
        }

        public void Update(T entity)
        {
            _unitOfWork.Entry(entity).State = EntityState.Modified;
            _unitOfWork.SaveChanges();
        }

        public void Delete(T entity)
        {
            _unitOfWork.Set<T>().Remove(entity);
            _unitOfWork.SaveChanges();
        }
    }
}

﻿using System;
using System.Configuration;

namespace CrudBasic.Infrastructure.Data
{
    public static class ConnectionString
    {
        public static string Get
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(value)) return value;

                if (ConfigurationManager.ConnectionStrings.Count == 0)
                    throw new Exception("Não foi encontrado nenhuma connection string no arquivo de configuração");

                var machineConnectionString = ConfigurationManager.ConnectionStrings["conexao@" + Environment.MachineName];
                return machineConnectionString != null ? "conexao@" + Environment.MachineName : "conexao";
            }
        }
        private static string value;
    }
}

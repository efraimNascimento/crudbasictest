namespace CrudBasic.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Vendas",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Data = c.DateTime(nullable: false),
                        ClienteId = c.Long(nullable: false),
                        TotalVenda = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clientes", t => t.ClienteId, cascadeDelete: true)
                .Index(t => t.ClienteId);
            
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Nome = c.String(),
                        Email = c.String(),
                        Endereco = c.String(),
                        Bairro = c.String(),
                        Cidade = c.String(),
                        Uf = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Produtos",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CodigoBarra = c.String(),
                        Descricao = c.String(),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VendaDetalhes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        VendaId = c.Long(nullable: false),
                        ProdutoId = c.Long(nullable: false),
                        Desconto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValorTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Produtos", t => t.ProdutoId, cascadeDelete: true)
                .ForeignKey("dbo.Vendas", t => t.VendaId, cascadeDelete: true)
                .Index(t => t.VendaId)
                .Index(t => t.ProdutoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VendaDetalhes", "VendaId", "dbo.Vendas");
            DropForeignKey("dbo.VendaDetalhes", "ProdutoId", "dbo.Produtos");
            DropForeignKey("dbo.Vendas", "ClienteId", "dbo.Clientes");
            DropIndex("dbo.VendaDetalhes", new[] { "ProdutoId" });
            DropIndex("dbo.VendaDetalhes", new[] { "VendaId" });
            DropIndex("dbo.Vendas", new[] { "ClienteId" });
            DropTable("dbo.VendaDetalhes");
            DropTable("dbo.Produtos");
            DropTable("dbo.Clientes");
            DropTable("dbo.Vendas");
        }
    }
}

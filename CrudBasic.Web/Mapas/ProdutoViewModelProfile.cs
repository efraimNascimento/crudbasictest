﻿using AutoMapper;
using CrudBasic.Core.DTO;
using CrudBasic.Web.Models;

namespace CrudBasic.Web.Mapas
{
    public class ProdutoViewModelProfile : Profile
    {
        public ProdutoViewModelProfile()
        {
            CreateMap<ProdutoDTO, ProdutoViewModel>().ReverseMap();
        }
    }
}
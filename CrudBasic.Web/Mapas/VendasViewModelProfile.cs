﻿using AutoMapper;
using CrudBasic.Core.DTO;
using CrudBasic.Web.Models;

namespace CrudBasic.Web.Mapas
{
    public class VendasViewModelProfile : Profile
    {
        public VendasViewModelProfile()
        {
            CreateMap<VendaDTO, VendaViewModel>().ReverseMap();
            CreateMap<VendaDetalheDTO, VendaDetalheViewModel>().ReverseMap();
        }
    }
}

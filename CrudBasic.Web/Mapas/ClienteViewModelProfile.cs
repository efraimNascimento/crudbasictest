﻿using AutoMapper;
using CrudBasic.Core.DTO;
using CrudBasic.Web.Models;

namespace CrudBasic.Web.Mapas
{
    public class ClienteViewModelProfile:Profile
    {
        public ClienteViewModelProfile()
        {
            CreateMap<ClienteDTO, ClienteViewModel>().ReverseMap();
        }
    }
}
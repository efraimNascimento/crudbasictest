﻿(function () {
    var ns = window.CB.Models;
    var self = this;
    ns.ClienteEditViewModel = ns.Model.extend({
        init: function (data) {
            data = data || {};
            this._super(data);
            self.messageConfirmacao = ko.observable();
            self.confirmacao = function (textoConfirmacao, confirma, naoConfirma) {
                self.messageConfirmacao(textoConfirmacao);
                $("#dialog-confirm").dialog({
                    resizable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                        "Confirmar": function () {
                            $(this).dialog("close");
                            confirma();
                        },
                        "Cancelar": function () {
                            $(this).dialog("close");
                            naoConfirma();
                        }
                    }
                }).dialog("open")
            };
        },
        Salvar: function () {
            self.confirmacao("Confirma os dados digitados?",
                function () {
                    $("form").submit();
                },
                function () {}); 
        },
        Delete: function () {
            self.confirmacao("Confirma a Exclusão das informações?",
                function () {
                    $("form").submit();
                },
                function () { });
        },
        Cancelar: function () {
            self.confirmacao("Deseja realmente descartar os dados digitados?", function () { window.location.href = "Index"; }, function () { }); 
        }
    });
}());
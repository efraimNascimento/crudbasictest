﻿/// <reference path="ClienteEditViewModel.js" />
(function () {
    var ns = window.CB.Models;
    var self = this;
    ns.ClienteViewModel = ns.Model.extend({
        init: function (data) {
            self = this;
            data = data || {};
            this._super(data);
        }
    });
}());
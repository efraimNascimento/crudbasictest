﻿window.CB = window.CrudBasic = window.crudbasic = {
    types: {},
    utils: {}
};

(function ($, ko, Class) {

    crudbasic.Models = {
        Model: Class.extend({
            init: function () { /**/ },
            singleEmptyInstance: function () {
                return this._emptyInstance = this._emptyInstance || new this.constructor();
            },
            isEmpty: function () {
                var jsonClean = ko.toJSON(this.singleEmptyInstance());
                var jsonObj = ko.toJSON(this);

                return jsonClean === jsonObj;
            },
            toFormUrlEncoded: function () {
                var js = ko.toJS(this);
                delete js.Enums;
                return jsonToFormUrlencoded(JSON.parse(ko.toJSON(js)));
            },
            updateModel: function (data) {
                data = ko.toJS(data);
                var _isModel;

                this._updatingModel = true;

                for (var field in data) {
                    var prop = this[field];

                    if (ko.isWriteableObservable(prop)) {
                        _isModel = isModel(prop.type);
                        if (_isModel && prop.isObservableArray) {

                            prop(crudbasic.mapListToCrudBasicModels(data[field], prop.type));
                        } else if (_isModel) {

                            updatePropModel(prop, new prop.type(data[field]));
                        } else {
                            prop(data[field]);
                        }
                    }
                }

                delete this._updatingModel;
            },
            clearModel: function () {
                ///<summary> deleta todas as propriedades que são models e que tem o Id não definido </summary>
                var that = ko.toJS(this);

                for (var field in that) {
                    var propValue = that[field];

                    if (propValue && propValue.hasOwnProperty("Id"))
                        if (propValue.Id == undefined)
                            delete that[field];
                        else
                            that[field] = propValue.clearModel();
                    else if (propValue && propValue instanceof Array) {
                        if (propValue.length == 1 && that[field].type) {
                            var objInstance = propValue[0];
                            var anotherObject = ko.toJS(new that[field].type());
                            if (objInstance.isEquals(anotherObject))
                                delete that[field];
                        }
                    } else if (propValue && !(propValue instanceof Date) && typeof propValue == "object" && !propValue.clearModel) {
                        delete that[field];
                    }


                }

                return that;
            }
        })
    };

    function updatePropModel(prop, instance) {

        if (prop())
            prop().updateModel(instance);
        else
            prop(instance);
    }

    function isModel(ctor) {

        return $.isFunction(ctor) && (crudbasic.Models.Model.prototype.isPrototypeOf(ctor.prototype));
    }

    crudbasic.mapListToCrudBasicModels = function (list, model, defaulItem) {
        if (((!list) || (!Array.isArray(list)) || list.length == 0))
            list = defaulItem ? [defaulItem] : [];

        var modelFn = (String.isString(model)) ? crudbasic.Models[model] : model;

        var result = [];
        list.forEach(function (item) {
            var instance = new modelFn(/*item*/);
            instance.updateModel(item);
            result.push(instance);
        });

        return result;
    };

    CB.getJSON = function (url, then) {
        $.getJSON(url, then).error(showError);
    };

    function showError(resp) {
        var errorObj = $.parseJSON(resp.responseText);
        if (errorObj['Errors'])
            $.showErros($.isArray(errorObj.Errors) ? errorObj.Errors : [errorObj]);
        else
            $.showErros($.isArray(errorObj) ? errorObj : [errorObj]);
    }
})(jQuery, ko, Class);
﻿using AutoMapper;
using CrudBasic.Core.Interfaces.Services;
using CrudBasic.Core.Mapas;
using CrudBasic.Core.Services;
using CrudBasic.Web.Mapas;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace CrudBasic.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperCoreConfiguration.Configure(new List<Profile>() { new ClienteViewModelProfile(), new ProdutoViewModelProfile(), new VendasViewModelProfile() });
        }
    }
}

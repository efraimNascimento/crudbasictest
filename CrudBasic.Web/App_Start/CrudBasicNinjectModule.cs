﻿using CrudBasic.Core.Interfaces.Services;
using CrudBasic.Core.Services;
using Ninject.Modules;

namespace CrudBasic.Web.App_Start
{
    public class CrudBasicNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IClientesServices>().To<ClientesServices>();
        }
    }
}
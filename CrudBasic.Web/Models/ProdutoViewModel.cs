﻿using System.ComponentModel.DataAnnotations;

namespace CrudBasic.Web.Models
{
    public class ProdutoViewModel
    {
        public long Id { get; set; }
        public string CodigoBarra { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace CrudBasic.Web.Models
{
    public class ClienteViewModel
    {
        public long Id { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Uf { get; set; }
    }
}
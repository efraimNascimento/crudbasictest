﻿using System.ComponentModel.DataAnnotations;

namespace CrudBasic.Web.Models
{
    public class VendaDetalheViewModel
    {
        public long Id { get; set; }
        [Required]
        public virtual ProdutoViewModel Produto { get; set; }
        public decimal Desconto { get; set; }
        [Required]
        public decimal ValorTotal { get; set; }
    }
}

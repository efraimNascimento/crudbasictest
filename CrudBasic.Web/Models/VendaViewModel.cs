﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CrudBasic.Web.Models
{
    public class VendaViewModel
    {
        public long Id { get; set; }
        [Required]
        public DateTime Data { get; set; }
        [Required]
        public ClienteViewModel Cliente { get; set; }
        public IEnumerable<VendaDetalheViewModel> DetalheDaVenda { get; set; }
        [Required]
        public decimal TotalVenda { get; set; }
    }
}

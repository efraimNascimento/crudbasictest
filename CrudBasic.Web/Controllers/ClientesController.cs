﻿using AutoMapper;
using CrudBasic.Core.DTO;
using CrudBasic.Core.Interfaces.Services;
using CrudBasic.Web.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CrudBasic.Web.Controllers
{
    public class ClientesController : Controller
    {
        private readonly IClientesServices _clientesServices;
        public ClientesController(IClientesServices clientesServices) => _clientesServices = clientesServices;

        public ActionResult Index() => View(Mapper.Map<IEnumerable<ClienteViewModel>>(_clientesServices.ListarTodos()));

        public ActionResult Details(int id) => View(Mapper.Map<ClienteViewModel>(_clientesServices.GetById(id)));

        public ActionResult Create() => View();

        [HttpPost]
        public ActionResult Create(ClienteViewModel cliente)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _clientesServices.Criar(Mapper.Map<ClienteDTO>(cliente));
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int id) => View(Mapper.Map<ClienteViewModel>(_clientesServices.GetById(id)));

        [HttpPost]
        public ActionResult Edit(int id, ClienteViewModel cliente)
        {
            try
            {
                _clientesServices.Alterar(Mapper.Map<ClienteDTO>(cliente));
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id) => View(Mapper.Map<ClienteViewModel>(_clientesServices.GetById(id)));

        [HttpPost]
        public ActionResult Delete(int id, ClienteViewModel cliente)
        {
            try
            {
                _clientesServices.Excluir(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

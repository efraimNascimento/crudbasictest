﻿using AutoMapper;
using CrudBasic.Core.DTO;
using CrudBasic.Core.Interfaces.Services;
using CrudBasic.Web.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CrudBasic.Web.Controllers
{
    public class ProdutosController : Controller
    {
        private readonly IProdutosService _produtosServices;

        public ProdutosController(IProdutosService produtosServices) => _produtosServices = produtosServices;

        public ActionResult Index() => View(Mapper.Map<IEnumerable<ProdutoViewModel>>(_produtosServices.ListarTodos()));

        public ActionResult Details(int id) => View(Mapper.Map<ProdutoViewModel>(_produtosServices.GetById(id)));

        public ActionResult Create() => View();

        [HttpPost]
        public ActionResult Create(ProdutoViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _produtosServices.Criar(Mapper.Map<ProdutoDTO>(model));
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int id) => View(Mapper.Map<ProdutoViewModel>(_produtosServices.GetById(id)));

        [HttpPost]
        public ActionResult Edit(int id, ProdutoViewModel model)
        {
            try
            {
                _produtosServices.Alterar(Mapper.Map<ProdutoDTO>(model));
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id) => View(Mapper.Map<ProdutoViewModel>(_produtosServices.GetById(id)));

        [HttpPost]
        public ActionResult Delete(int id, ProdutoViewModel model)
        {
            try
            {
                _produtosServices.Excluir(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}